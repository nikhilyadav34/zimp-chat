# Zimp Chat App #

Zimp Chat App.

### What is this repository for? ###

* Nodejs legacy
* Version 6.0.0

### How do I get set up? ###

* Clone Project
* Install packages
```
npm install
```

* Copy example.env in root directory and add configurations
```
sudo cp example.env .env
```
* To run node built in server
```
npm start
```
* Node build in server will work on port 3000
* Browse
* http://localhost:3000